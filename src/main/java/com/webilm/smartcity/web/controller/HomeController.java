package com.webilm.smartcity.web.controller;

import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.webilm.smartcity.dto.User;
import com.webilm.smartcity.security.SecurityUtils;
import com.webilm.smartcity.service.IAuthenticationService;

/**
 * 
 * @author apuravchauhan
 *
 */
@Controller
public class HomeController {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private IAuthenticationService authService;
	@Autowired
	private ObjectMapper mapper;
	@Value("${digitaltown_login}")
	private String loginUrl;

	@PostConstruct
	public void postConstruct() {
		System.out.println("Via syso " + loginUrl);
		logger.debug("Home controller construction complete: ", loginUrl);
	}

	@RequestMapping("/privacy")
	public String privacy() {
		return "privacy";
	}

	@RequestMapping("/")
	public String home() {
		return "redirect:/login";
	}

	@RequestMapping("/open-login")
	public String homeOpen() {
		return "redirect:" + loginUrl;
	}

	@RequestMapping("/login")
	public String login(Map<String, Object> model) {
		model.put("loginUrl", loginUrl);
		return "login";
	}

	@RequestMapping(value = "/auth", method = RequestMethod.GET)
	public String getCode(@RequestParam(required = false) String code, Map<String, Object> model) {
		logger.debug("Code received in auth", code);
		if (StringUtils.isBlank(code)) {
			model.put("user", "{}");
			return "response";
		}
		try {
			User user = authService.loginByCode(code);
			if (user != null) {
				SecurityUtils.setLoggedInUser(user);
				model.put("user", mapper.writeValueAsString(user));
			}
		} catch (Exception ex) {
			logger.error("Exception validating code: ", ex);
		}
		return "response";

	}

}
