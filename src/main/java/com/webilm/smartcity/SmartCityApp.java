package com.webilm.smartcity;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.databind.DeserializationFeature;

/**
 * 
 * @author apuravchauhan
 *
 */
@SpringBootApplication
@ComponentScan(basePackages = "com.webilm.smartcity")
public class SmartCityApp extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(SmartCityApp.class);
	}
	@Bean
	public Jackson2ObjectMapperBuilder objectMapperBuilder() {
	    Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
	    builder.serializationInclusion(JsonInclude.Include.NON_NULL);
	    builder.featuresToDisable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
	    builder.featuresToEnable(Feature.ALLOW_UNQUOTED_FIELD_NAMES,Feature.ALLOW_SINGLE_QUOTES);
	    return builder;
	}


	@Bean
	public HttpClient httpClient() {
		PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
		cm.setMaxTotal(200);
		cm.setDefaultMaxPerRoute(50);

		CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(cm).disableCookieManagement().build();
		return httpClient;
	}

	public static void main(String[] args) {
		SpringApplication.run(SmartCityApp.class, args);
	}

}