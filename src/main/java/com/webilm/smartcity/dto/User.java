package com.webilm.smartcity.dto;

import java.util.Date;
import java.util.Set;

/**
 * 
 * @author apuravchauhan
 *
 */
public class User {

	public static enum Role {
		USR
	}

	private String id;
	private String session;
	private String name;
	private String mail;
	private String mob;
	private String pass;
	private Boolean locked;
	private Set<Role> role;
	private Date cr;

	public User() {
	}

	public Boolean getLocked() {
		return locked;
	}

	public void setLocked(Boolean locked) {
		this.locked = locked;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getMob() {
		return mob;
	}

	public void setMob(String mob) {
		this.mob = mob;
	}

	public Set<Role> getRole() {
		return role;
	}

	public String getPassword() {
		return pass;
	}

	public String getUsername() {
		return mail;
	}

	public boolean isAccountNonExpired() {
		return true;
	}

	public boolean isAccountNonLocked() {
		Boolean locked = this.locked;
		if (locked != null)
			return !locked;
		return true;
	}

	public boolean isCredentialsNonExpired() {
		return true;
	}

	public boolean isEnabled() {
		return true;
	}

	public Date getCr() {
		return cr;
	}

	public void setCr(Date cr) {
		this.cr = cr;
	}

	public String getSession() {
		return session;
	}

	public void setSession(String session) {
		this.session = session;
	}

}
