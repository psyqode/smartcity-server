package com.webilm.smartcity.service;

import com.webilm.smartcity.dto.User;

/**
 * 
 * @author apurav.chauhan
 *
 */
public interface IAuthenticationService {

	
	public User loginByCode(String code) throws RuntimeException;

	public void logout();

}
