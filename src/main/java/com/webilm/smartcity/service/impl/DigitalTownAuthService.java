package com.webilm.smartcity.service.impl;

import java.util.Map;
import java.util.Objects;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.webilm.smartcity.dto.User;
import com.webilm.smartcity.service.IAuthenticationService;

/**
 * 
 * @author apuravchauhan
 *
 */
@Service
public class DigitalTownAuthService implements IAuthenticationService {
	private final Logger logger = org.slf4j.LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ObjectMapper mapper;
	@Autowired
	private HttpClient httpClient;
	@Value("${digitaltown_clientid}")
	private String clientId;
	@Value("${digitaltown_clientsecret}")
	private String clientSecret;
	@Value("${digitaltown_redirectURI}")
	private String redirectURI;

	@Override
	public void logout() {
		// TODO Auto-generated method stub

	}

	@Override
	public User loginByCode(String code) {
		HttpPost httppost = new HttpPost("https://api.digitaltown.com/sso/token");
		User user = null;
		try {
			StringEntity payload = new StringEntity("{\"grant_type\":\"authorization_code\",\"code\":\"" + code
					+ "\",\"client_id\":\"" + clientId + "\",\"client_secret\":\"" + clientSecret
					+ "\",\"redirect_uri\":\"" + redirectURI + "\"}");
			payload.setContentType("application/json");
			httppost.setEntity(payload);

			HttpResponse response = httpClient.execute(httppost);;
			HttpEntity entity = response.getEntity();
			String accessToken = "";
			if (entity != null) {
				// EntityUtils to get the response content
				String content = EntityUtils.toString(entity);
				System.out.println(content);
				Map<String, Object> map = mapper.readValue(content, new TypeReference<Map<String, Object>>() {
				});
				/*
				 * token_type expires_in access_token refresh_token
				 */
				accessToken = Objects.toString(map.get("access_token"),null);
				// get user details from access token
				HttpGet get = new HttpGet("https://api.digitaltown.com/sso/users");
				get.setHeader("authorization", "Bearer " + accessToken);
				response = httpClient.execute(get);
				entity = response.getEntity();

				if (entity != null) {
					// EntityUtils to get the response content
					content = EntityUtils.toString(entity);
					System.out.println(content + " ");
					map = mapper.readValue(content, new TypeReference<Map<String, Object>>() {
					});
					
					String id = Objects.toString(map.get("id"),null);
					String firstName = Objects.toString(map.get("first_name"),null);
					String lastName = Objects.toString(map.get("last_name"),null);
					String email = Objects.toString(map.get("email"),null);
					user = new User();
					user.setId(id);
					user.setName(firstName + " " + lastName);
					user.setSession(accessToken);
					user.setMail(email);
				}
			}
		} catch (Exception ex) {
			logger.error("Exception get details from code: ", ex);
			throw new RuntimeException(ex);
		}
		return user;
	}

}
